# TicTacToe Game by Junseong and Jonathan

gameboard = [[" ", " ", " "], 
            [" ", " ", " "], 
            [" ", " ", " "]]

def print_board():
    # print the current state of the gameboard
    
    print(gameboard[0][0],"|",gameboard[0][1],"|",gameboard[0][2])
    print("--|---|--")
    print(gameboard[1][0],"|", gameboard[1][1],"|", gameboard[1][2])
    print("--|---|--")
    print(gameboard[2][0],"|",gameboard[2][1],"|",gameboard[2][2])
    
def place(piece, choice):
    # place the piece on the gameboard
    # piece = "X" or piece = "O"
    
    x_choice = int(choice[1])
    y_choice = int(choice[3])
    
    if x_choice < 0 or x_choice > 2 or y_choice < 0 or y_choice > 2:
        print("Illegal move")(3,3)
        return place(gameboard, piece)
    elif gameboard[y_choice][x_choice] != " ":
        print("There is already a piece!!!!")
        return False
    else:
        gameboard[y_choice][x_choice] = piece
        print_board()
        return True
        
def is_full(gameboard):
    # return True if gameboard if full, otherwise return False
    
    for row in gameboard:
        for square in row:
            if square == " ":
                return False
    return True()

def start_game(gameboard):
    # start the game, and continue until it is over
    
    print("Welcome to TicTacToe")
    print_board(gameboard)
    
    curr = "O"
    while not is_full(gameboard) and not row_win(gameboard) and not col_win(gameboard) and not diggy_win(gameboard):
        
        place(gameboard, curr)
        
        if curr == "O":
            curr = "X"
        else:
            curr = "O"
            
    if not is_full(gameboard) and curr == "O":
        print("Game Over, X Wins!")
    elif not is_full(gameboard) and curr == "X":
        print("Game Over, O Wins!")
    else:
        print("Game Over, Tie Game!")
    
def row_win(gameboard):
    # return True if someone has won from 3 in a row, otherwise False
    
    for row in gameboard:
        if row[0] == row[1] == row[2] and row[0] != " ":
            return True
        
    return False

def col_win(gameboard):
    # return True if someone has won from 3 in a column, otherwise False
    
    for col in range(3):
        if gameboard[0][col] == gameboard[1][col] == gameboard[2][col] and gameboard[0][col] != " ":
            return True
    
    return False

def diggy_win(gameboard):
    # return True if diggy, otherwise False
    
    if gameboard[0][0] == gameboard[1][1] == gameboard[2][2] and gameboard[0][0] != " ":
        return True
    if gameboard[2][0] == gameboard[1][1] == gameboard[0][2] and gameboard[2][0] != " ":
        return True

    return False

def clear_board():
    
    gameboard = [[" ", " ", " "], 
            [" ", " ", " "], 
            [" ", " ", " "]]